# _DLL Consulta RUC directo SUNAT_

## Contenido
- Solución .NET
- Proyecto .NET Framework 4.5
- Proyecto .NET Standard 2.0
- Proyecto de Pruebas Framework
- Proyecto de Pruebas ASP Net Core


## Paquetes Nuget

```sh
- Ninguno []
```
## Videos Relacionados
| Nombre | Enlace |
| ------ | ------ |
| DLL Consulta RUC directo SUNAT | ▶️[Enlace](https://www.youtube.com/watch?v=cQMFX9zCaQc) |

## Redes Sociales
| Plugin | README |
| ------ | ------ |
| Página de Facebook | 👥 [Enlace](https://www.facebook.com/MundoVisualStudio) |
| Facebook | 👥 [Enlace](https://www.facebook.com/danimedina159) |
| Telegram | 💬 [Enlace](https://t.me/MundoVisualStudio) |



## License
[GoldTech](https://corp-goldtech.com/)
