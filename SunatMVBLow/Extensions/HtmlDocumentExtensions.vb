﻿Imports System.Runtime.CompilerServices
Imports System.Text.RegularExpressions
Imports System.Windows.Forms

Module HtmlDocumentExtensions
    <Extension()>
    Public Function ByCoreSingle(doc As HtmlDocument, Optional tag As String = "") As HtmlElement
        Dim Lista = HtmlDocumentShared.IntermediateFilterNodes(doc, doc.All, tag)
        Dim NodeSingle As HtmlElement = Nothing
        If Lista.Count > 0 Then
            NodeSingle = Lista(0)
        End If
        Return NodeSingle
    End Function
    <Extension()>
    Public Function ByCore(doc As HtmlDocument, Optional tag As String = "") As List(Of HtmlElement)
        Return HtmlDocumentShared.IntermediateFilterNodes(doc, doc.All, tag)
    End Function
    <Extension()>
    Public Function ByClass(doc As HtmlDocument, Optional tag As String = "") As List(Of HtmlElement)
        Dim AResult = HtmlDocumentShared.RegexByClass(tag)
        Return HtmlDocumentShared.FilterNodes(doc, doc.All, AResult(0), "className", AResult(1))
    End Function

End Module
