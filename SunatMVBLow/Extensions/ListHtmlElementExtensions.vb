﻿Imports System.Runtime.CompilerServices
Imports System.Windows.Forms

Module ListHtmlElementExtensions
    <Extension()>
    Public Function Elements(lista As List(Of HtmlElement), name As String) As List(Of HtmlElement)
        Return HtmlDocumentShared.Elements(lista, name)
    End Function
    <Extension()>
    Public Function fGet(Lista As List(Of HtmlElement), Index As Integer) As String
        Dim ValorNode As String = "-"
        Try
            Dim selectNode = Lista(Index)
            'If selectNode IsNot Nothing Then ValorNode = UI.ClearNode(selectNode.InnerText)
            If selectNode IsNot Nothing Then
                Dim TEXTO = selectNode.InnerText
                Dim TEXTOPARCIAL = ""
                If TEXTO.Contains("<br>") Then
                    Dim ATexto = TEXTO.Split("<br>")
                    TEXTOPARCIAL = ATexto(0)
                ElseIf TEXTO.Contains("<BR>") Then
                    Dim ATexto = TEXTO.Split("<BR>")
                    TEXTOPARCIAL = ATexto(0)
                ElseIf TEXTO.Contains(vbCrLf) Then
                    Dim ATexto = TEXTO.Split(vbCrLf)
                    TEXTOPARCIAL = ATexto(0)
                Else
                    TEXTOPARCIAL = TEXTO
                End If
                ValorNode = UI.ClearNode(TEXTOPARCIAL)
            End If
        Catch ex As Exception
            ValorNode = ex.Message
        End Try
        Return ValorNode
    End Function
    <Extension()>
    Public Function fGetList(Lista As List(Of HtmlElement), Index As Integer) As List(Of String)
        Dim selectNode = Lista(Index).Elements("//tr")
        Dim List As New List(Of String) From {"-"}
        If selectNode IsNot Nothing Then
            List = (From Item In selectNode Select UI.ClearNode(Item.InnerText)).ToList
        End If
        Return List
    End Function
    <Extension()>
    Public Function fGetList2(Lista As List(Of HtmlElement), Index As Integer) As List(Of String)
        Dim selectNode = Lista(Index)
        Dim List As New List(Of String) From {"-"}
        If selectNode IsNot Nothing Then
            List = (From Item In selectNode.InnerText.Split(",") Select UI.ClearNode(Item)).ToList
        End If
        Return List
    End Function
End Module
