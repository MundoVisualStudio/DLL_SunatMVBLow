﻿Imports System.Runtime.CompilerServices
Imports System.Text.RegularExpressions
Imports System.Windows.Forms

Module HtmlElementExtensions
    <Extension()>
    Public Function ByCoreSingle(ele As HtmlElement, Optional tag As String = "") As HtmlElement
        Dim Lista = HtmlDocumentShared.IntermediateFilterNodes(ele, ele.Children, tag)
        Dim NodeSingle As HtmlElement = Nothing
        If Lista.Count > 0 Then
            NodeSingle = Lista(0)
        End If
        Return NodeSingle
    End Function
    <Extension()>
    Public Function ByCore(ele As HtmlElement, Optional tag As String = "") As List(Of HtmlElement)
        Return HtmlDocumentShared.IntermediateFilterNodes(ele, ele.Children, tag)
    End Function
    <Extension()>
    Public Function ByClass(ele As HtmlElement, Optional tag As String = "") As List(Of HtmlElement)
        Dim AResult = HtmlDocumentShared.RegexByClass(tag)
        Return HtmlDocumentShared.FilterNodes(ele, ele.Children, AResult(0), "className", AResult(1))
    End Function
    <Extension()>
    Public Function Element(ele As HtmlElement, name As String) As HtmlElement
        Dim FoundElement As HtmlElement = Nothing
        For Each Item As HtmlElement In ele.Children
            If Item.TagName.ToLower() = name.ToLower() Then
                FoundElement = Item
                Exit For
            End If
        Next
        Return FoundElement
    End Function
    <Extension()>
    Public Function Elements(ele As HtmlElement, name As String) As List(Of HtmlElement)
        Dim List = New List(Of HtmlElement) From {ele}
        Return HtmlDocumentShared.Elements(List, name)
        'Dim FoundList As New List(Of HtmlElement)
        'If name.StartsWith("//") Then
        '    Dim newname = name.Replace("//", "")
        '    For Each Item As HtmlElement In ele.Children
        '        If Item.Children.Count > 0 Then
        '            For Each ItemChild As HtmlElement In Item.Children
        '                FoundList.AddRange(ItemChild.Elements(name))
        '            Next
        '        Else
        '            If Item.TagName.ToLower() = newname.ToLower() Then
        '                FoundList.Add(Item)
        '            End If
        '        End If
        '    Next
        'Else
        '    For Each Item As HtmlElement In ele.Children
        '        If Item.Children.Count > 0 Then
        '            For Each ItemChild As HtmlElement In Item.Children
        '                If ItemChild.TagName.ToLower() = name.ToLower() Then
        '                    FoundList.Add(ItemChild)
        '                End If
        '            Next
        '        Else
        '            If Item.TagName.ToLower() = name.ToLower() Then
        '                FoundList.Add(Item)
        '            End If
        '        End If
        '    Next
        'End If
        'Return FoundList
    End Function
End Module
