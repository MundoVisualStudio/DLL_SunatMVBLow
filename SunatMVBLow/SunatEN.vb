﻿Public Class SunatEN
    Public Property success As Boolean = False
    Public Property mensaje As String = ""
    Public Property ruc As String
    Public Property razon_social As String
    Public Property tipo_contribuyente As String
    Public Property tipo_documento As String
    Public Property nombre_comercial As String
    Public Property fecha_inscripcion As String
    Public Property fecha_inicio_actividades As String
    Public Property estado_contribuyente As String
    Public Property condicion_contribuyente As String
    Public Property domicilio_fiscal As String
    Public Property sistema_emision_comprobante As String
    Public Property actividad_comercio_exterior As String
    Public Property sistema_contabilidad As String
    Public Property actividades_economicas As List(Of String)
    Public Property comprobante_pago As List(Of String)
    Public Property sistema_emision_electronica As List(Of String)
    Public Property emisor_electronico_desde As String
    Public Property comprobantes_electronicos As List(Of String)
    Public Property afiliado_ple As String
    Public Property padrones As List(Of String)
    Public Property representantes_legales As List(Of SunatRepresentanteLegalEN) = New List(Of SunatRepresentanteLegalEN)
    Public Property cantidad_trabajadores As List(Of SunatCantidadTrabajadoresEN) = New List(Of SunatCantidadTrabajadoresEN)
    Public Property establecimientos_anexo As List(Of SunatEstablecimientosEN) = New List(Of SunatEstablecimientosEN)
End Class
