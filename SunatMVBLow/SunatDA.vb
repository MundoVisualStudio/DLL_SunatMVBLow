﻿Imports System.Configuration
Imports System.Dynamic
Imports System.Net
Imports System.Net.Http
Imports System.Text.RegularExpressions
Imports System.Threading.Tasks
Imports System.Web

Public Class SunatDA
    '<ConfigurationProperty("alias2", DefaultValue:="alias.txt", IsRequired:=True, IsKey:=False), RegexStringValidator("\w+\S*")>
    'Public Property Alias2() As String
    '    Get
    '        Return CStr(Me("alias2"))
    '    End Get
    '    Set(ByVal value As String)
    '        Me("alias2") = value
    '    End Set
    'End Property

    Private Const URLCaptcha As String = "https://e-consultaruc.sunat.gob.pe/cl-ti-itmrconsruc/captcha?accion=random"
    Private Const URLJCR As String = "https://e-consultaruc.sunat.gob.pe/cl-ti-itmrconsruc/jcrS00Alias"
    Public Shared Async Function ConsultaSUNAT(ruc As String) As Task(Of String)
        Dim Resultado = ""
        Dim ff As Object = ""
        Dim objE As New SunatEN
        Dim token As String = ""
        Try
            Dim cookies As New CookieContainer()
            'Dim DATA = System.IO.File.ReadAllText("D:\demoRUC.txt")
            Dim DATA = ""
            Using client = UI.CreateHttpClientSUNAT(cookies)
                Dim result As HttpResponseMessage = Await client.GetAsync(URLCaptcha)
                If result.IsSuccessStatusCode Then
                    token = Await result.Content.ReadAsStringAsync()
                    Dim pairs As New Dictionary(Of String, String) From {{"accion", "consPorRuc"}, {"nroRuc", ruc}, {"contexto", "ti-it"}, {"modo", "1"}, {"numRnd", token}}
                    result = Await client.PostAsync(URLJCR, New FormUrlEncodedContent(pairs))
                    If result.IsSuccessStatusCode Then
                        DATA = HttpUtility.HtmlDecode(Await result.Content.ReadAsStringAsync())
                    End If
                End If
            End Using
            If DATA.Contains("Debe verificar el número y volver a ingresar") Then
                objE.mensaje = "El RUC ingresado no es válido."
            Else
                Dim doc = WebBrowserShared.CreateHtmlDocument(DATA)
                Dim listNode = doc.ByCoreSingle("div[@class='panel panel-primary']")?.ByCore("div[@class='row']").Elements("//p|//table")
                listNode.Insert(0, doc.ByCoreSingle("div[@class='col-sm-7']").Element("h4"))
                Dim v0 = listNode.fGet(0)
                Dim ARuc = v0.Split("-")
                If ARuc.Length = 2 Then
                    objE.ruc = UI.ValueTrim(ARuc(0))
                    objE.razon_social = UI.ValueTrim(ARuc(1))
                Else
                    objE.ruc = UI.ValueTrim(v0)
                End If
                If ruc.StartsWith(10) Then
                    objE.tipo_contribuyente = listNode.fGet(1)
                    objE.tipo_documento = listNode.fGet(2)
                    objE.nombre_comercial = listNode.fGet(3)
                    objE.fecha_inscripcion = listNode.fGet(4)
                    objE.fecha_inicio_actividades = listNode.fGet(5)
                    objE.estado_contribuyente = listNode.fGet(6)
                    objE.condicion_contribuyente = listNode.fGet(7)
                    objE.domicilio_fiscal = listNode.fGet(8)
                    objE.sistema_emision_comprobante = listNode.fGet(9)
                    objE.actividad_comercio_exterior = listNode.fGet(10)
                    objE.sistema_contabilidad = listNode.fGet(11)
                    objE.actividades_economicas = listNode.fGetList(12)
                    objE.comprobante_pago = listNode.fGetList(13)
                    If listNode.Count > 18 Then
                        objE.sistema_emision_electronica = listNode.fGetList(14)
                        objE.emisor_electronico_desde = listNode.fGet(15)
                        objE.comprobantes_electronicos = listNode.fGetList2(16)
                        objE.afiliado_ple = listNode.fGet(17)
                        objE.padrones = listNode.fGetList(18)
                    Else
                        objE.sistema_emision_electronica = listNode.fGetList(14)
                        objE.emisor_electronico_desde = listNode.fGet(14)
                        objE.comprobantes_electronicos = listNode.fGetList2(15)
                        objE.afiliado_ple = listNode.fGet(16)
                        objE.padrones = listNode.fGetList(17)
                    End If
                Else
                    objE.tipo_contribuyente = listNode.fGet(1)
                    objE.nombre_comercial = listNode.fGet(2)
                    objE.fecha_inscripcion = listNode.fGet(3)
                    objE.fecha_inicio_actividades = listNode.fGet(4)
                    objE.estado_contribuyente = listNode.fGet(5)
                    objE.condicion_contribuyente = listNode.fGet(6)
                    objE.domicilio_fiscal = listNode.fGet(7)
                    objE.sistema_emision_comprobante = listNode.fGet(8)
                    objE.actividad_comercio_exterior = listNode.fGet(9)
                    objE.sistema_contabilidad = listNode.fGet(10)
                    objE.actividades_economicas = listNode.fGetList(11)
                    objE.comprobante_pago = listNode.fGetList(12)
                    objE.sistema_emision_electronica = listNode.fGetList(13)
                    objE.emisor_electronico_desde = listNode.fGet(14)
                    objE.comprobantes_electronicos = listNode.fGetList2(15)
                    objE.afiliado_ple = listNode.fGet(16)
                    objE.padrones = listNode.fGetList(17)
                    'Representante Legal
                    Using clientRL = UI.CreateHttpClientSUNAT(cookies)
                        Dim pairsRL As New Dictionary(Of String, String) From {{"accion", "getRepLeg"}, {"contexto", "ti-it"}, {"modo", "1"}, {"desRuc", objE.razon_social}, {"nroRuc", ruc}, {"numRnd", token}}
                        Dim resultRL = Await clientRL.PostAsync(URLJCR, New FormUrlEncodedContent(pairsRL))
                        If resultRL.IsSuccessStatusCode Then
                            Dim DATARL As String = HttpUtility.HtmlDecode(Await resultRL.Content.ReadAsStringAsync())
                            Dim docRL = WebBrowserShared.CreateHtmlDocument(DATARL)
                            Dim listNodeRL = docRL.ByCoreSingle("//table[@class='table']")?.ByCoreSingle("tbody").Elements("tr")
                            For Each itemRL In listNodeRL
                                'Dim lstTDsRL = RegexShared.GetValues(itemRL, "td")
                                Dim dyObjRL As New SunatRepresentanteLegalEN
                                dyObjRL.tipo_documento = UI.ValueTrim(itemRL.Children(0).InnerText)
                                dyObjRL.numero_documento = UI.ValueTrim(itemRL.Children(1).InnerText)
                                dyObjRL.nombre = UI.ValueTrim(itemRL.Children(2).InnerText)
                                dyObjRL.cargo = UI.ValueTrim(itemRL.Children(3).InnerText)
                                dyObjRL.fecha_desde = UI.ValueTrim(itemRL.Children(4).InnerText)
                                objE.representantes_legales.Add(dyObjRL)
                            Next
                        End If
                    End Using
                    'Cantidad de Trabajadores CT
                    Using clientCT = UI.CreateHttpClientSUNAT(cookies)
                        Dim pairsCT As New Dictionary(Of String, String) From {{"accion", "getCantTrab"}, {"contexto", "ti-it"}, {"modo", "1"}, {"desRuc", objE.razon_social}, {"nroRuc", ruc}, {"numRnd", token}}
                        Dim resultCT = Await clientCT.PostAsync(URLJCR, New FormUrlEncodedContent(pairsCT))
                        If resultCT.IsSuccessStatusCode Then
                            Dim DATARL As String = HttpUtility.HtmlDecode(Await resultCT.Content.ReadAsStringAsync())
                            Dim docRL = WebBrowserShared.CreateHtmlDocument(DATARL)
                            Dim listNodeRL = docRL.ByCoreSingle("//table[@class='table']")?.ByCoreSingle("tbody").Elements("tr")
                            For Each itemRL In listNodeRL
                                Dim dyObjRL As New SunatCantidadTrabajadoresEN
                                dyObjRL.periodo = UI.ValueTrim(itemRL.Children(0).InnerText)
                                dyObjRL.numero_trabajadores = UI.ValueTrim(itemRL.Children(1).InnerText)
                                dyObjRL.numero_pensionistas = UI.ValueTrim(itemRL.Children(2).InnerText)
                                dyObjRL.numero_prestadores_servicio = UI.ValueTrim(itemRL.Children(3).InnerText)
                                objE.cantidad_trabajadores.Add(dyObjRL)
                            Next
                        End If
                    End Using
                    'Establecimientos Anexos EA
                    Using clientEA = UI.CreateHttpClientSUNAT(cookies)
                        Dim pairsEA As New Dictionary(Of String, String) From {{"accion", "getLocAnex"}, {"contexto", "ti-it"}, {"modo", "1"}, {"desRuc", objE.razon_social}, {"nroRuc", ruc}, {"numRnd", token}}
                        Dim resultEA = Await clientEA.PostAsync(URLJCR, New FormUrlEncodedContent(pairsEA))
                        If resultEA.IsSuccessStatusCode Then
                            Dim DATARL As String = HttpUtility.HtmlDecode(Await resultEA.Content.ReadAsStringAsync())
                            Dim docRL = WebBrowserShared.CreateHtmlDocument(DATARL)
                            Dim listNodeRL = docRL.ByCoreSingle("//table[@class='table']")?.ByCoreSingle("tbody").Elements("tr")
                            For Each itemRL In listNodeRL
                                Dim dyObjRL As New SunatEstablecimientosEN
                                dyObjRL.codigo = UI.ValueTrim(itemRL.Children(0).InnerText)
                                dyObjRL.tipo_establecimiento = UI.ValueTrim(itemRL.Children(1).InnerText)
                                dyObjRL.direccion = UI.ValueTrim(itemRL.Children(2).InnerText)
                                dyObjRL.actividad_economica = UI.ValueTrim(itemRL.Children(3).InnerText)
                                objE.establecimientos_anexo.Add(dyObjRL)
                            Next
                        End If
                    End Using
                End If
                objE.success = True
            End If
            ff = "dqwdqwd"
        Catch ex As Exception
            objE.mensaje = ex.Message
        End Try
        'Resultado = JSONSerializer(Of SunatEN).Serialize(objE)
        'Dim serializador As New System.Runtime.Serialization.Json.DataContractJsonSerializer(seat.GetType)
        'Dim ms As New System.IO.MemoryStream
        'serializador.WriteObject(ms, seat)
        'Dim textoJson As String = System.Text.Encoding.UTF8.GetString(ms.ToArray)

        Dim serializador As New System.Web.Script.Serialization.JavaScriptSerializer
        Dim sb As New System.Text.StringBuilder
        Resultado = serializador.Serialize(objE)

        Return Resultado
        'Return objE
    End Function
End Class

'Dim pairs As List(Of KeyValuePair(Of String, String)) = New List(Of KeyValuePair(Of String, String))() From {
'    New KeyValuePair(Of String, String)("accion", "consPorRuc"),
'    New KeyValuePair(Of String, String)("nroRuc", ruc),
'    New KeyValuePair(Of String, String)("contexto", "ti-it"),
'    New KeyValuePair(Of String, String)("modo", "1"),
'    New KeyValuePair(Of String, String)("numRnd", token)
'}