﻿Imports System.Windows.Forms

Public Class WebBrowserShared
    Public Shared Function CreateHtmlDocument(DATA As String) As HtmlDocument
        Dim browser = New WebBrowser()
        browser.ScriptErrorsSuppressed = True
        browser.DocumentText = DATA
        browser.Document.OpenNew(True)
        browser.Document.Write(DATA)
        browser.Refresh()
        Return browser.Document
    End Function
End Class
