﻿Imports System.Net
Imports System.Net.Http

Public Class UI
    Public Shared Function Value(ByVal Valor As Object, ByVal Optional DefaultValue As String = "") As String
        Dim NuevoValor As String

        Try
            NuevoValor = Convert.ToString(Valor)

            If Valor = "" OrElse Valor Is Nothing OrElse Valor Is DBNull.Value Then
                NuevoValor = DefaultValue
            End If

        Catch ex As Exception
            NuevoValor = DefaultValue
        End Try

        Return NuevoValor
    End Function

    Public Shared Function ValueTrim(ByVal Valor As Object, ByVal Optional DefaultValue As String = "") As String
        Dim NuevoValor As String = UI.Value(Valor, DefaultValue)
        NuevoValor = NuevoValor.Trim()
        Return NuevoValor
    End Function
    Public Shared Function ClearNode(node As String) As String
        node = node.Replace(vbCrLf, "").Trim
        node = node.Replace(vbTab, "")
        node = node.Replace(New String(" ", 10), " ")
        node = node.Replace(New String(" ", 9), " ")
        node = node.Replace(New String(" ", 8), " ")
        node = node.Replace(New String(" ", 7), " ")
        node = node.Replace(New String(" ", 6), " ")
        node = node.Replace(New String(" ", 5), " ")
        node = node.Replace(New String(" ", 4), " ")
        node = node.Replace(New String(" ", 3), " ")
        node = node.Replace(New String(" ", 2), " ")
        'node = node.Replace("  ", "")
        'Node = Regex.Replace(Node, "[^\w\.@-]", String.Empty)
        Return node
    End Function
    Public Shared Function CreateHttpClientSUNAT(cookies As CookieContainer) As HttpClient
        Dim client As HttpClient = New HttpClient(New HttpClientHandler() With {.CookieContainer = cookies, .UseCookies = True, .AllowAutoRedirect = True})
        client.DefaultRequestHeaders.Add("User-Agent", "Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.150 Safari/537.36")
        client.DefaultRequestHeaders.Add("accept", "*/*")
        client.DefaultRequestHeaders.Add("Connection", "keep-alive")
        client.DefaultRequestHeaders.Add("Host", "e-consultaruc.sunat.gob.pe")
        ServicePointManager.ServerCertificateValidationCallback = Function(se, cert, chain, sslerror) True
        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls Or SecurityProtocolType.Tls11 Or SecurityProtocolType.Tls12
        Return client
    End Function
End Class
