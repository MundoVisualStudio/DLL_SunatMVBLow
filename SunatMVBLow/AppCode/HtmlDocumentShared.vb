﻿Imports System.Text.RegularExpressions
Imports System.Windows.Forms

Public Class HtmlDocumentShared
    Public Enum FilterMode
        Equals
        Contains
    End Enum
    Public Shared Function RegexByCore(Optional tag As String = "")
        Dim matchtag = Regex.Match(tag, "^([\w\-]+)")
        Dim matchattr_value As Match = Regex.Match(tag, "(\w+)=[""']([a-zA-Z0-9_.\s+-:'""]+)[""']")
        Dim _tag As String = ""
        Dim _attr As String = ""
        Dim _value As String = ""
        If matchtag.Success Then
            _tag = matchtag.Value
        End If
        If matchattr_value.Success Then
            _attr = matchattr_value.Groups(1).Value
            _value = matchattr_value.Groups(2).Value
        End If
        Dim AResult = {_tag, _attr, _value}
        Return AResult
    End Function
    Public Shared Function RegexByClass(Optional tag As String = "")
        Dim matchtag = Regex.Match(tag, "^([\w\-]+)")
        Dim matchclass As Match = Regex.Match(tag, "(?<=\bclass=')[^']*")
        Dim tagValue As String = ""
        Dim classValue As String = ""
        If matchtag.Success Then
            tagValue = matchtag.Value
        End If
        If matchclass.Success Then
            classValue = matchclass.Value
        End If
        Dim AResult = {tagValue, classValue}
        Return AResult
    End Function
    Public Shared Function IntermediateFilterNodes(doc_or_ele As Object, elements As HtmlElementCollection, tag As String, Optional filtro As FilterMode = FilterMode.Equals) As List(Of HtmlElement)
        Dim AResult = HtmlDocumentShared.RegexByCore(tag)
        Dim vAttr = AResult(1)
        If vAttr = "class" Then
            vAttr = "className"
        End If
        Return HtmlDocumentShared.FilterNodes(doc_or_ele, elements, AResult(0), vAttr, AResult(2), filtro)
    End Function
    Public Shared Function FilterNodes(doc_or_ele As Object, elements As HtmlElementCollection, tag As String, attr As String, value As String, Optional filtro As FilterMode = FilterMode.Equals) As List(Of HtmlElement)
        Dim lst As List(Of HtmlElement) = New List(Of HtmlElement)()
        Dim empty_tag As Boolean = String.IsNullOrEmpty(tag)
        Dim empty_cn As Boolean = String.IsNullOrEmpty(attr)
        If empty_tag AndAlso empty_cn Then Return lst
        Dim elmts As HtmlElementCollection = If(empty_tag, elements, doc_or_ele.GetElementsByTagName(tag))
        'Dim elmts As HtmlElementCollection = elements
        'If Not empty_tag Then
        '    If TypeOf doc_or_ele Is HtmlDocument Then

        '    ElseIf TypeOf doc_or_ele Is HtmlElement Then
        '        doc_or_ele.GetElementsByTagName(tag)
        '    End If
        'End If
        If empty_cn Then
            lst.AddRange(elmts.Cast(Of HtmlElement)())
            Return lst
        End If
        Select Case filtro
            Case FilterMode.Equals
                For i As Integer = 0 To elmts.Count - 1
                    If elmts(i).GetAttribute(attr) = value Then
                        lst.Add(elmts(i))
                    End If
                Next
                Exit Select
            Case FilterMode.Contains
                For i As Integer = 0 To elmts.Count - 1
                    If elmts(i).GetAttribute(attr).Contains(value) Then
                        lst.Add(elmts(i))
                    End If
                Next
                Exit Select
        End Select

        Return lst
    End Function
    Public Shared Function Elements(lista As List(Of HtmlElement), name As String) As List(Of HtmlElement)
        Dim FoundList As New List(Of HtmlElement)
        name = name.ToLower()
        Dim AName = name.Split("|")
        If AName.All(Function(x) x.StartsWith("//")) Then
            For Each Item As HtmlElement In lista
                For Each ItemChild In Item.All
                    If AName.Any(Function(y) y.Contains(ItemChild.TagName.ToLower())) Then
                        FoundList.Add(ItemChild)
                    End If
                Next
            Next
        Else
            For Each _name In AName
                If _name.StartsWith("//") Then
                    Dim newname = _name.Replace("//", "")
                    For Each Item As HtmlElement In lista
                        For Each ItemChild In Item.All
                            If ItemChild.TagName.ToLower() = newname.ToLower() Then
                                FoundList.Add(ItemChild)
                            End If
                        Next
                    Next
                Else
                    For Each Item As HtmlElement In lista
                        If Item.Children.Count > 0 Then
                            For Each ItemChild As HtmlElement In Item.Children
                                If ItemChild.TagName.ToLower() = _name.ToLower() Then
                                    FoundList.Add(ItemChild)
                                End If
                            Next
                        Else
                            If Item.TagName.ToLower() = _name.ToLower() Then
                                FoundList.Add(Item)
                            End If
                        End If
                    Next
                End If
            Next
        End If
        Return FoundList
    End Function
End Class
