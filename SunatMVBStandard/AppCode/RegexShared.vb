﻿Imports System.Text.RegularExpressions

Public Class RegexShared
    Public Shared Function GetTag(This As String, Tag As String) As String
        Dim regex As New Regex($"<\s*{Tag}[^>]*>(.*?)<\s*/\s*{Tag}>", RegexOptions.Singleline)
        Dim results As New Text.StringBuilder
        For Each m As Match In regex.Matches(This)
            results.Append(m.Value)
        Next
        Return results.ToString()
    End Function
    Public Shared Function GetTags(This As String, Tag As String) As List(Of String)
        Dim CleanThis = RegexShared.GetTag(This, Tag)
        'Dim CleanThis = This
        CleanThis = UI.ValueTrimFull(CleanThis)
        Dim results As New List(Of String)
        'Dim findtext2 As String = $"<\s*{Tag}[^>]*>(.*?)<\s*/\s*{Tag}>"
        Dim findtext2 As String = $"<{Tag}>(?<data>[\s\S]*?)<\/{Tag}>"
        Dim doregex2 As MatchCollection = Regex.Matches(CleanThis, findtext2)
        'Falta "corregir" pero funciona con ClearTags
        For Each item As Match In doregex2
            results.Add(item.Value)
        Next
        Return results
    End Function

    Public Shared Function GetValues(This As String, Tag As String) As List(Of String)
        Dim CleanThis = RegexShared.GetTag(This, Tag)
        'Dim CleanThis = This
        CleanThis = UI.ValueTrimFull(CleanThis)
        Dim results As New List(Of String)
        'Dim findtext2 As String = $"<\s*{Tag}[^>]*>(.*?)<\s*/\s*{Tag}>"
        Dim findtext2 As String = $"(?<=<{Tag}.*?>)(.*?)(?=</{Tag}>)"
        Dim doregex2 As MatchCollection = Regex.Matches(CleanThis, findtext2)
        'Falta "corregir" pero funciona con ClearTags
        For Each item As Match In doregex2
            results.Add(RegexShared.ClearTags(item.Value))
        Next
        Return results
    End Function

    Public Shared Function ClearTags(This As String) As String
        Dim tagRemove As Regex = New Regex("<[^>]*(>|$)")
        Dim result As String = tagRemove.Replace(This, String.Empty)
        Return result
        'Dim regex2 As New Text.RegularExpressions.Regex("<.*?>", RegexOptions.Singleline)
        'Dim result As String = regex2.Replace(fila, String.Empty)
    End Function

    Public Shared Function ParseToListSUNAT(This As String, RUC As String) As List(Of String)
        Dim results As New List(Of String)
        Dim LRH4 = Regex.Matches(This, $"<(h4\s*class=""list-group-item-heading"")>{RUC}(?<data>[\s\S]*?)<\/(h4)>")
        Dim LRPTABLE = Regex.Matches(This, "<(p\s*class=""list-group-item-text""|table\s*class=""table\stblResultado"")>(?<data>[\s\S]*?)<\/(p|table)>")
        For Each Item As Match In LRH4
            results.Add(Item.Value)
        Next
        For Each Item As Match In LRPTABLE
            results.Add(Item.Value)
        Next
        Return results
    End Function

    Public Shared Function ParseToListSUNAT2(This As String, RUC As String) As List(Of String)
        Dim results As New List(Of String)
        Dim LRH4 = Regex.Matches(This, $"<(h4\s*class=""list-group-item-heading"")>{RUC}(?<data>[\s\S]*?)<\/(h4)>")
        Dim LRPTABLE = Regex.Matches(This, "<(p\s*class=""list-group-item-text""|table\s*class=""table"")>(?<data>[\s\S]*?)<\/(p|table)>")
        For Each Item As Match In LRH4
            results.Add(Item.Value)
        Next
        For Each Item As Match In LRPTABLE
            results.Add(Item.Value)
        Next
        Return results
    End Function

End Class

'<(b \ s *class="boldtitle"|span\s*id="desc")>(.*)</(b|span)>

'<(div \ s *class="panel\spanel-primary")>(.*)</(div)>

'<(div \ s *class="col-sm-5"|div\s*class="col-sm-7"|div\s*class="col-sm-3")>(?<data>[\s\S]*?)<\/(div|div|div)>
'<(div \ s *class="col-sm-5"|div\s*class="col-sm-7"|div\s*class="col-sm-3")>(?<data>[\s\S]*?)<\/(div)>

'<(h4 \ s *class="list-group-item-heading"|p\s*class="list-group-item-text"|table\s*class="table\stblResultado")>(?<data>[\s\S]*?)<\/(h4|p|table)>
