﻿Imports System.Runtime.CompilerServices
Imports System.Text.RegularExpressions

Module StringExtensions
    <Extension()>
    Public Function TrimComment(This As String) As String
        'Dim comments1 As Regex = New Regex("//.*?$", RegexOptions.Multiline)
        Dim comments2 As Regex = New Regex("\s*/\*.*?\*/\s*", RegexOptions.Singleline)
        Dim comments3 As Regex = New Regex("\s*<!--.*?-->\s*", RegexOptions.Singleline)
        This = comments2.Replace(This, "")
        This = comments3.Replace(This, "")
        Return This
    End Function
End Module
