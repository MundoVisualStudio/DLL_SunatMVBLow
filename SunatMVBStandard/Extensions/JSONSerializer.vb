﻿Imports System.IO
Imports System.Runtime.Serialization.Json
Imports System.Text

Public Class JSONSerializer(Of TType As {Class, New})
    Public Shared Function Serialize(instance As TType) As String
        Dim serializer = New DataContractJsonSerializer(GetType(TType))
        Using stream = New MemoryStream()
            serializer.WriteObject(stream, instance)
            'Return Encoding.[Default].GetString(stream.ToArray())
            Return Encoding.UTF8.GetString(stream.ToArray())
        End Using
    End Function

    Public Shared Function Deserialize(json As String) As TType
        Using stream = New MemoryStream(Encoding.[Default].GetBytes(json))
            Dim serializer = New DataContractJsonSerializer(GetType(TType))
            Return TryCast(serializer.ReadObject(stream), TType)
        End Using
    End Function
End Class
