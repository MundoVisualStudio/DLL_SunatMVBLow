﻿Imports System.Runtime.CompilerServices

Module ListStringExtensions
    <Extension()>
    Public Function fGet(This As List(Of String), Index As Integer) As String
        Dim Resultado As String
        Try
            Dim valor As String = This(Index)
            Resultado = UI.ValueTrimFull(RegexShared.ClearTags(valor))
        Catch ex As Exception
            Resultado = ex.Message
        End Try
        Return Resultado
    End Function
    <Extension()>
    Public Function fGetList(This As List(Of String), Index As Integer) As List(Of String)
        Dim Resultado As New List(Of String)
        Try
            Dim valor As String = This(Index)
            Resultado = RegexShared.GetValues(valor, "td")
        Catch ex As Exception
        End Try
        Return Resultado
    End Function
    <Extension()>
    Public Function fGetList2(This As List(Of String), Index As Integer) As List(Of String)
        Dim Resultado As New List(Of String)
        Try
            Dim valor As String = RegexShared.ClearTags(UI.ValueTrimFull(This(Index)))
            Dim AValor = valor.Split(","c)
            For Each Item In AValor
                Resultado.Add(Item)
            Next
        Catch ex As Exception
        End Try
        Return Resultado
    End Function
End Module
