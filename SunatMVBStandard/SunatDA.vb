﻿Imports System.Configuration
Imports System.Dynamic
Imports System.IO
Imports System.Net
Imports System.Net.Http
Imports System.Net.Security
Imports System.Text
Imports System.Text.RegularExpressions
Imports System.Threading.Tasks
Imports System.Web
Imports System.Xml

Public Class SunatDA
	'<ConfigurationProperty("alias2", DefaultValue:="alias.txt", IsRequired:=True, IsKey:=False), RegexStringValidator("\w+\S*")>
	'Public Property Alias2() As String
	'    Get
	'        Return CStr(Me("alias2"))
	'    End Get
	'    Set(ByVal value As String)
	'        Me("alias2") = value
	'    End Set
	'End Property
	Private Const URLCaptcha As String = "https://e-consultaruc.sunat.gob.pe/cl-ti-itmrconsruc/captcha?accion=random"
	Private Const URLJCR As String = "https://e-consultaruc.sunat.gob.pe/cl-ti-itmrconsruc/jcrS00Alias"
	Public Shared Async Function ConsultaSUNAT(RUC As String) As Task(Of String)
		Dim ff As Object = ""
		Dim objE As New SunatEN
		Dim Resultado As String
		Dim token As String = ""
		Try
			Dim cookies As New CookieContainer()
			'Dim DATA = System.IO.File.ReadAllText("D:\demoRUC.txt")
			Dim DATA = ""
			Using client = UI.CreateHttpClientSUNAT(cookies)
				Dim result As HttpResponseMessage = Await client.GetAsync(URLCaptcha)
				If result.IsSuccessStatusCode Then
					token = Await result.Content.ReadAsStringAsync()
					Dim pairs As New Dictionary(Of String, String) From {{"accion", "consPorRuc"}, {"nroRuc", RUC}, {"contexto", "ti-it"}, {"modo", "1"}, {"numRnd", token}}
					result = Await client.PostAsync(URLJCR, New FormUrlEncodedContent(pairs))
					If result.IsSuccessStatusCode Then
						DATA = HttpUtility.HtmlDecode(Await result.Content.ReadAsStringAsync())
					End If
				End If
			End Using
			If DATA.Contains("Debe verificar el número y volver a ingresar") Then
				objE.mensaje = "El RUC ingresado no es válido."
			Else
				Dim HTMLClean = DATA.TrimComment()
				Dim LResult = RegexShared.ParseToListSUNAT(HTMLClean, RUC)
				Dim v0 = LResult.fGet(0)
				Dim ARuc = v0.Split("-"c)
				If ARuc.Length = 2 Then
					objE.ruc = UI.ValueTrimFull(ARuc(0))
					objE.razon_social = UI.ValueTrimFull(ARuc(1))
				Else
					objE.ruc = UI.ValueTrimFull(v0)
				End If
				If RUC.StartsWith("10") Then
					objE.tipo_contribuyente = LResult.fGet(1)
					objE.tipo_documento = LResult.fGet(2)
					objE.nombre_comercial = LResult.fGet(3)
					objE.fecha_inscripcion = LResult.fGet(4)
					objE.fecha_inicio_actividades = LResult.fGet(5)
					objE.estado_contribuyente = LResult.fGet(6)
					objE.condicion_contribuyente = LResult.fGet(7)
					objE.domicilio_fiscal = LResult.fGet(8)
					objE.sistema_emision_comprobante = LResult.fGet(9)
					objE.actividad_comercio_exterior = LResult.fGet(10)
					objE.sistema_contabilidad = LResult.fGet(11)
					objE.actividades_economicas = LResult.fGetList(12)
					objE.comprobante_pago = LResult.fGetList(13)
					If LResult.Count > 18 Then
						objE.sistema_emision_electronica = LResult.fGetList(14)
						objE.emisor_electronico_desde = LResult.fGet(15)
						objE.comprobantes_electronicos = LResult.fGetList2(16)
						objE.afiliado_ple = LResult.fGet(17)
						objE.padrones = LResult.fGetList(18)
					Else
						objE.sistema_emision_electronica = LResult.fGetList(14)
						objE.emisor_electronico_desde = LResult.fGet(14)
						objE.comprobantes_electronicos = LResult.fGetList2(15)
						objE.afiliado_ple = LResult.fGet(16)
						objE.padrones = LResult.fGetList(17)
					End If
				Else
					objE.tipo_contribuyente = LResult.fGet(1)
					objE.nombre_comercial = LResult.fGet(2)
					objE.fecha_inscripcion = LResult.fGet(3)
					objE.fecha_inicio_actividades = LResult.fGet(4)
					objE.estado_contribuyente = LResult.fGet(5)
					objE.condicion_contribuyente = LResult.fGet(6)
					objE.domicilio_fiscal = LResult.fGet(7)
					objE.sistema_emision_comprobante = LResult.fGet(8)
					objE.actividad_comercio_exterior = LResult.fGet(9)
					objE.sistema_contabilidad = LResult.fGet(10)
					objE.actividades_economicas = LResult.fGetList(11)
					objE.comprobante_pago = LResult.fGetList(12)
					objE.sistema_emision_electronica = LResult.fGetList(13)
					objE.emisor_electronico_desde = LResult.fGet(14)
					objE.comprobantes_electronicos = LResult.fGetList2(15)
					objE.afiliado_ple = LResult.fGet(16)
					objE.padrones = LResult.fGetList(17)
					'Representante Legal
					Using clientRL = UI.CreateHttpClientSUNAT(cookies)
						Dim pairsRL As New Dictionary(Of String, String) From {{"accion", "getRepLeg"}, {"contexto", "ti-it"}, {"modo", "1"}, {"desRuc", objE.razon_social}, {"nroRuc", RUC}, {"numRnd", token}}
						Dim resultRL = Await clientRL.PostAsync(URLJCR, New FormUrlEncodedContent(pairsRL))
						If resultRL.IsSuccessStatusCode Then
							Dim DATARL As String = HttpUtility.HtmlDecode(Await resultRL.Content.ReadAsStringAsync())
							Dim HTMLCleanRL = DATARL.TrimComment()
							Dim LResultRL = RegexShared.ParseToListSUNAT2(HTMLCleanRL, RUC)
							Dim strTBody = RegexShared.GetTag(LResultRL(0), "tbody")
							If Not String.IsNullOrEmpty(strTBody) AndAlso Not String.IsNullOrWhiteSpace(strTBody) Then
								Dim lstTRsRL = RegexShared.GetTags(strTBody, "tr")
								For Each itemRL In lstTRsRL
									Dim lstTDsRL = RegexShared.GetValues(itemRL, "td")
									Dim dyObjRL As New SunatRepresentanteLegalEN
									dyObjRL.tipo_documento = UI.ValueTrimFull(lstTDsRL(0))
									dyObjRL.numero_documento = UI.ValueTrimFull(lstTDsRL(1))
									dyObjRL.nombre = UI.ValueTrimFull(lstTDsRL(2))
									dyObjRL.cargo = UI.ValueTrimFull(lstTDsRL(3))
									dyObjRL.fecha_desde = UI.ValueTrimFull(lstTDsRL(4))
									objE.representantes_legales.Add(dyObjRL)
								Next
							End If
						End If
					End Using
					'Cantidad de Trabajadores CT
					Using clientCT = UI.CreateHttpClientSUNAT(cookies)
						Dim pairsCT As New Dictionary(Of String, String) From {{"accion", "getCantTrab"}, {"contexto", "ti-it"}, {"modo", "1"}, {"desRuc", objE.razon_social}, {"nroRuc", RUC}, {"numRnd", token}}
						Dim resultCT = Await clientCT.PostAsync(URLJCR, New FormUrlEncodedContent(pairsCT))
						If resultCT.IsSuccessStatusCode Then
							Dim DATARL As String = HttpUtility.HtmlDecode(Await resultCT.Content.ReadAsStringAsync())
							Dim HTMLCleanRL = DATARL.TrimComment()
							Dim strTBody = RegexShared.GetTag(HTMLCleanRL, "tbody")
							If Not String.IsNullOrEmpty(strTBody) AndAlso Not String.IsNullOrWhiteSpace(strTBody) Then
								Dim lstTRsRL = RegexShared.GetTags(strTBody, "tr")
								For Each itemRL In lstTRsRL
									Dim lstTDsRL = RegexShared.GetValues(itemRL, "td")
									Dim dyObjRL As New SunatCantidadTrabajadoresEN
									dyObjRL.periodo = UI.ValueTrimFull(lstTDsRL(0))
									dyObjRL.numero_trabajadores = UI.ValueTrimFull(lstTDsRL(1))
									dyObjRL.numero_pensionistas = UI.ValueTrimFull(lstTDsRL(2))
									dyObjRL.numero_prestadores_servicio = UI.ValueTrimFull(lstTDsRL(3))
									objE.cantidad_trabajadores.Add(dyObjRL)
								Next
							End If
						End If
					End Using
					'Establecimientos Anexos EA
					Using clientEA = UI.CreateHttpClientSUNAT(cookies)
						Dim pairsEA As New Dictionary(Of String, String) From {{"accion", "getLocAnex"}, {"contexto", "ti-it"}, {"modo", "1"}, {"desRuc", objE.razon_social}, {"nroRuc", RUC}, {"numRnd", token}}
						Dim resultEA = Await clientEA.PostAsync(URLJCR, New FormUrlEncodedContent(pairsEA))
						If resultEA.IsSuccessStatusCode Then
							Dim DATARL As String = HttpUtility.HtmlDecode(Await resultEA.Content.ReadAsStringAsync())
							Dim HTMLCleanRL = DATARL.TrimComment()
							Dim strTBody = RegexShared.GetTag(HTMLCleanRL, "tbody")
							If Not String.IsNullOrEmpty(strTBody) AndAlso Not String.IsNullOrWhiteSpace(strTBody) Then
								Dim lstTRsRL = RegexShared.GetTags(strTBody, "tr")
								For Each itemRL In lstTRsRL
									Dim lstTDsRL = RegexShared.GetValues(itemRL, "td")
									Dim dyObjRL As New SunatEstablecimientosEN
									dyObjRL.codigo = UI.ValueTrimFull(lstTDsRL(0))
									dyObjRL.tipo_establecimiento = UI.ValueTrimFull(lstTDsRL(1))
									dyObjRL.direccion = UI.ValueTrimFull(lstTDsRL(2))
									dyObjRL.actividad_economica = UI.ValueTrimFull(lstTDsRL(3))
									objE.establecimientos_anexo.Add(dyObjRL)
								Next
							End If
						End If
					End Using
				End If
				objE.success = True
			End If
		Catch ex As Exception
			objE.mensaje = ex.Message
		End Try
		Resultado = JSONSerializer(Of SunatEN).Serialize(objE)
		Return Resultado
		'Return Resultado
	End Function
	'Public Shared Async Function obtenerRuc(ruc As String) As Task(Of String)
	Public Shared Function obtenerRuc(ruc As String) As String
		Dim ff As Object = ""
		Dim rpta As String = ""
		Dim cookies As CookieContainer = New CookieContainer()

		Using client As HttpClient = New HttpClient(CType(New HttpClientHandler() With {
			.CookieContainer = cookies,
			.UseCookies = True,
			.AllowAutoRedirect = True
		}, HttpMessageHandler))

			Try
				'client.DefaultRequestHeaders.Add("User-Agent", "Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.150 Safari/537.36")
				'client.DefaultRequestHeaders.Add("accept", "*/*")
				'client.DefaultRequestHeaders.Add("Connection", "keep-alive")
				'client.DefaultRequestHeaders.Add("Host", "e-consultaruc.sunat.gob.pe")
				'ServicePointManager.ServerCertificateValidationCallback = CType((Function(se, cert, chain, sslerror) True), RemoteCertificateValidationCallback)
				'ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls Or SecurityProtocolType.Tls11 Or SecurityProtocolType.Tls12
				'Dim url As String = "https://e-consultaruc.sunat.gob.pe/cl-ti-itmrconsruc/captcha?accion=random"
				'Dim result As HttpResponseMessage = Await client.GetAsync(New Uri(url))

				'If result.IsSuccessStatusCode Then
				'Dim token As String = Await result.Content.ReadAsStringAsync()
				'Dim pairs As New Dictionary(Of String, String) From {{"accion", "consPorRuc"}, {"nroRuc", ruc}, {"contexto", "ti-it"}, {"modo", "1"}, {"numRnd", token}}
				'Dim content As FormUrlEncodedContent = New FormUrlEncodedContent(CType(pairs, IEnumerable(Of KeyValuePair(Of String, String))))
				'url = "https://e-consultaruc.sunat.gob.pe/cl-ti-itmrconsruc/jcrS00Alias"
				'result = Await client.PostAsync(New Uri(url), CType(content, HttpContent))
				'If result.IsSuccessStatusCode Then
				Dim DataRuc As List(Of String) = New List(Of String)()
				'Dim html As String = Await HttpUtility.HtmlDecode(result.Content.ReadAsStringAsync())
				Dim html As String = HttpUtility.HtmlDecode(IO.File.ReadAllText("D:\demoRUC20.txt")) '36 17
				'Dim html As String = HttpUtility.HtmlDecode(IO.File.ReadAllText("D:\demoRUC10.txt")) '38 18
				'Dim html As String = HttpUtility.HtmlDecode(IO.File.ReadAllText("D:\demoRUC10E.txt")) '37 17
				'Dim html As String = HttpUtility.HtmlDecode(IO.File.ReadAllText("D:\demoRUC10R.txt")) '37 17
				'html = UI.ValueTrimFull(html)
				html = html.TrimComment()
				Dim ii As Integer = 0
				Dim objE As New SunatEN
				'<(div\s*class="panel panel-primary")>(.*)</div>
				'ruc = "10092438819"
				'ruc = "10454545457"
				ruc = "20433661495"
				Dim LResult = RegexShared.ParseToListSUNAT(html, ruc)

				If ruc.StartsWith("10") Then
					objE.ruc = LResult.fGet(0)
					objE.tipo_contribuyente = LResult.fGet(1)
					objE.tipo_documento = LResult.fGet(2)
					objE.nombre_comercial = LResult.fGet(3)
					objE.fecha_inscripcion = LResult.fGet(4)
					objE.fecha_inicio_actividades = LResult.fGet(5)
					objE.estado_contribuyente = LResult.fGet(6)
					objE.condicion_contribuyente = LResult.fGet(7)
					objE.domicilio_fiscal = LResult.fGet(8)
					objE.sistema_emision_comprobante = LResult.fGet(9)
					objE.actividad_comercio_exterior = LResult.fGet(10)
					objE.sistema_contabilidad = LResult.fGet(11)
					objE.actividades_economicas = LResult.fGetList(12)
					objE.comprobante_pago = LResult.fGetList(13)
					If LResult.Count > 18 Then
						objE.sistema_emision_electronica = LResult.fGetList(14)
						objE.emisor_electronico_desde = LResult.fGet(15)
						objE.comprobantes_electronicos = LResult.fGetList2(16)
						objE.afiliado_ple = LResult.fGet(17)
						objE.padrones = LResult.fGetList(18)
					Else
						objE.sistema_emision_electronica = LResult.fGetList(14)
						objE.emisor_electronico_desde = LResult.fGet(14)
						objE.comprobantes_electronicos = LResult.fGetList2(15)
						objE.afiliado_ple = LResult.fGet(16)
						objE.padrones = LResult.fGetList(17)
					End If
				Else
					objE.ruc = LResult.fGet(0)
					objE.tipo_contribuyente = LResult.fGet(1)
					objE.nombre_comercial = LResult.fGet(2)
					objE.fecha_inscripcion = LResult.fGet(3)
					objE.fecha_inicio_actividades = LResult.fGet(4)
					objE.estado_contribuyente = LResult.fGet(5)
					objE.condicion_contribuyente = LResult.fGet(6)
					objE.domicilio_fiscal = LResult.fGet(7)
					objE.sistema_emision_comprobante = LResult.fGet(8)
					objE.actividad_comercio_exterior = LResult.fGet(9)
					objE.sistema_contabilidad = LResult.fGet(10)
					objE.actividades_economicas = LResult.fGetList(11)
					objE.comprobante_pago = LResult.fGetList(12)
					objE.sistema_emision_electronica = LResult.fGetList(13)
					objE.emisor_electronico_desde = LResult.fGet(14)
					objE.comprobantes_electronicos = LResult.fGetList2(15)
					objE.afiliado_ple = LResult.fGet(16)
					objE.padrones = LResult.fGetList(17)
				End If
				objE.success = True
				'ff = JSONSerializer(Of SunatEN).Serialize(objE)
				'rpta = String.Join("¬", CType(DataRuc, IEnumerable(Of String)))
				'End If
				'End If
				rpta = JSONSerializer(Of SunatEN).Serialize(objE)

			Catch ex As Exception
			End Try
		End Using

		Return rpta
	End Function
End Class

'Dim pairs As List(Of KeyValuePair(Of String, String)) = New List(Of KeyValuePair(Of String, String))() From {
'    New KeyValuePair(Of String, String)("accion", "consPorRuc"),
'    New KeyValuePair(Of String, String)("nroRuc", ruc),
'    New KeyValuePair(Of String, String)("contexto", "ti-it"),
'    New KeyValuePair(Of String, String)("modo", "1"),
'    New KeyValuePair(Of String, String)("numRnd", token)
'}

''<(b\s*class="boldtitle"|span\s*id="desc")>(.*)</(b|span)>
'Dim array As String() = {}
'array = Regex.Split(html, "<div class=""list-group-item")
'For index1 As Integer = 0 To array.Length - 1
'Dim fila As String = array(index1)
'ii = fila.IndexOf("list-group-item-heading")
'If ii > -1 AndAlso (Not fila.Contains("hidden-print""") OrElse fila.Contains("tblResultado")) Then

'ff = "qwdqw"
'Dim clean_fila = RegexShared.GetValues(fila, "h4")
'Dim clean_fila2 = RegexShared.GetValues(fila, "p")
'Dim clean_fila3 = RegexShared.GetValues(fila, "td")
'Dim clean_fila4 = RegexShared.GetValues(fila, "table")
'ff = "sasd"
'End If
'Next

'Dim filas As String() = html.Split(New String(0) {"list-group-item"""}, StringSplitOptions.None)
'Dim n As Integer = filas.Length
'Dim posClave As Integer = -1
'Dim posValor As Integer = -1
'Dim posMayor As Integer = -1
'Dim posMenor As Integer = -1
'For index1 As Integer = 0 To n - 1
'Dim fila As String = filas(index1)
'posClave = fila.IndexOf("list-group-item-heading")

'If posClave > -1 Then

'Dim clean_fila = RegexShared.GetValues(fila, "h4")

'Dim clean_fila2 = RegexShared.GetValues(fila, "p")

'posMayor = fila.IndexOf(">", posClave)
'If posMayor > -1 Then
'posMenor = fila.IndexOf("<", posMayor)
'Dim clave As String = fila.Substring(posMayor + 1, posMenor - posMayor - 1)
'posClave = If(index1 > 1, fila.IndexOf("list-group-item-text", posMenor), fila.IndexOf("list-group-item-heading", posMenor))
'Dim valor As String
'If posClave > -1 Then
'posMayor = fila.IndexOf(">", posClave)
'posMenor = fila.IndexOf("<", posMayor)
'valor = fila.Substring(posMayor + 1, posMenor - posMayor - 1)
'valor = UI.ValueTrimFull(valor)
'DataRuc.Add(clave & "|" & valor)
'posClave = fila.IndexOf("list-group-item-heading", posMenor)
'If posClave > -1 Then
'posMayor = fila.IndexOf(">", posClave)

'If posMayor > -1 Then
'posMenor = fila.IndexOf("<", posMayor)
'clave = fila.Substring(posMayor + 1, posMenor - posMayor - 1)
'posClave = fila.IndexOf("list-group-item-text", posMenor)
'posMayor = fila.IndexOf(">", posClave)
'posMenor = fila.IndexOf("<", posMayor)
'valor = fila.Substring(posMayor + 1, posMenor - posMayor - 1)
'valor = UI.ValueTrimFull(valor)
'DataRuc.Add(clave & "|" & valor)
'End If
'End If
'Else
'posClave = fila.IndexOf("tblResultado", posMenor)

'If posClave > -1 Then
'Dim str As String = fila.Substring(posClave, fila.Length - posClave)
'Dim stringList As List(Of String) = New List(Of String)()
'Dim strArray As String() = str.Split(New String(0) {"<tr>"}, StringSplitOptions.None)
'Dim length As Integer = strArray.Length

'For index2 As Integer = 0 To length - 1
'Dim num1 As Integer = strArray(index2).IndexOf("<td>")

'If num1 > -1 Then
'Dim num2 As Integer = strArray(index2).IndexOf("<", num1 + 1)
'Dim valoaux = UI.ValueTrimFull(strArray(index2).Substring(num1 + 4, num2 - (num1 + 4)))
'stringList.Add(valoaux)
'End If
'Next

'valor = String.Join(",", CType(stringList, IEnumerable(Of String)))
'DataRuc.Add(clave & "|" & valor)
'End If
'End If
'End If
'End If
'Next